<?php

namespace App\Http\Controllers;

use App\WerApp\Football\FootballLiga;
use App\WerApp\Football\FootballTeam;
use Illuminate\Http\Request;
use WerHelper;
use Illuminate\Support\Str;

class FootballLigaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if (isset($request->clean)) {
                $data = FootballLiga::all();
            } else {
                $ligas = FootballLiga::get();


                foreach ($ligas as $key => $liga) {
                    $ligas[$key]['teams']  = FootballTeam::where('liga_id', 'like', '%' . $liga->id . '%')->get();
                }

                $data = $ligas;
            }

            return  WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return $request;
        try {
            $baru = new FootballLiga(['id' => Str::uuid()]);
            $baru->fill($request->data);
            $simpan = $baru->save();
            return WerHelper::successResponse($simpan);
        } catch (\Throwable $th) {
            dd($th);
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WerApp\Football\Liga  $liga
     * @return \Illuminate\Http\Response
     */
    public function show($keyword)
    {

        try {
            // $data = FootballLiga::with('matchList')
            $data = FootballLiga::where('id', $keyword)
                ->orWhere('slug', $keyword)
                ->orWhere('slug_alt', $keyword)
                ->get();

            foreach ($data as $key => $liga) {
                $data[$key]['teams']  = FootballTeam::where('liga_id', 'like', '%' . $liga->id . '%')->get();
            }

            // return $data[0];
            return  WerHelper::successResponse($data[0]);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WerApp\Football\Liga  $liga
     * @return \Illuminate\Http\Response
     */
    public function edit(FootballLiga $liga)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WerApp\Football\Liga  $liga
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FootballLiga $liga)
    {
        $liga->fill($request->data);
        $liga->save();
        return $liga;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WerApp\Football\Liga  $liga
     * @return \Illuminate\Http\Response
     */
    public function destroy(FootballLiga $liga)
    {
        try {
            $data = $liga->delete();
            return  WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }
}
