<?php

namespace App\Http\Controllers;

use App\WerApp\Football\FootballLiga;
use App\WerApp\Football\FootballMatch;
use App\WerApp\Football\FootballTeam;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use WerHelper;

class FootballTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $team =  FootballTeam::all();
        // $team->load(['liga' => function ($query) {
        //     dd($query);
        // }]);
        // return  $data = FootballTeam::all();
        // // $team = FootballTeam::liga()->get();

        // return $team;

        try {
            // return $request;

            if (isset($request->clean)) {
                $data = FootballTeam::all();
                $arx = [];
                foreach ($data as $key => $team) {
                    if (is_iterable($team->liga_id)) {

                        $ligas =  FootballLiga::whereIn('id', $team->liga_id)->get();
                    } else {
                        $ligas =  FootballLiga::where('id', $team->liga_id)->get();
                    }

                    $data[$key]['liga'] = $ligas;
                }



                // return  $data = FootballTeam::with('liga')->get();
                // // $data = FootballTeam::with('liga')->get();
                // $data =        FootballTeam::get(function ($data) {
                //     return $data->toArray();
                // });
                // return $data;
            } else {
                $data = FootballTeam::with('liga', 'homeMatch', 'awayMatch')->orderBy('name', 'asc')->get();
            }
            // $data = FootballTeam::with('liga', 'homeMatch', 'awayMatch')->orderBy('name', 'asc')->get();

            // $key  = env('APP_KEY');
            // $data = ' namaku rendrian arma';
            // return $encryppted = Crypt::encrypt($data);
            // return $encryppted = Crypt::encrypt(WerHelper::successResponse($data));


            return WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $newLiga = [];
            foreach ($request->liga as $key => $liga) {
                array_push($newLiga, $liga['id']);
            }

            $data = [
                'id' => $request->id,
                'bg_color' => $request->bg_color,
                'liga_id' => $newLiga,
                'description' => $request->description,
                'logo' => $request->logo,
                'name' => $request->name,
                'name_alt' => $request->name_alt,
            ];
            $x = new FootballTeam($data);
            $x->save();

            return  WerHelper::successResponse($x);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WerApp\Football\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show($keyword)
    {
        try {
            $data = FootballTeam::with('liga', 'homeMatch', 'awayMatch')
                ->where('id', $keyword)
                ->orWhere('slug', $keyword)
                ->get();
            return  WerHelper::successResponse($data);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WerApp\Football\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(FootballTeam $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WerApp\Football\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FootballTeam $team)
    {

        try {
            $newLiga = [];
            foreach ($request->liga as $key => $liga) {
                array_push($newLiga, $liga['id']);
            }

            $data = [
                'liga_id' => $newLiga,
                'name' => $request->name,
                'logo' => $request->logo,

            ];

            $team->fill($data);
            $team->save();
            return WerHelper::successResponse($team);
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WerApp\Football\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(FootballTeam $team)
    {
        try {

            return WerHelper::successResponse($team->delete());
        } catch (\Throwable $th) {
            return WerHelper::errorResponse($th);
        }
    }
}
