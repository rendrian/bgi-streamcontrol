<?php

use App\WerApp\Football\FootballLiga;
use App\WerApp\Football\FootballTeam;
use App\WerApp\Football\FootballMatch;


class WerHelper
{

    public static function errorResponse($th)
    {

        // $errorResponse = self::errorSwitcher($th->getCode());
        return response([
            'success' => false,
            'error' => [
                'code' => $th->getCode(),
                'msg' => $th->getMessage()
            ]
        ], 201);
    }
    public static function successResponse($data)
    {
        return response([
            'success' => true,
            'data' => $data
        ], 200);
    }


    public static function  errorSwitcher($code)
    {

        $kode = (string) ($code);
        switch ($code) {
            case "23000":
                $errorResponse = 'Data Already Exist!';
                break;
            default:
                $errorResponse = null;
                break;

                return $errorResponse;
        }
    }


    public static function tanggal_indo($tanggal, $cetak_hari = false)
    {
        $hari = array(
            1 =>    'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        );

        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $split       = explode('-', $tanggal);
        $tgl_indo = (int) $split[2] . ' ' . $bulan[(int) $split[1]];

        if ($cetak_hari) {
            $num = date('N', strtotime($tanggal));
            return $hari[$num] . ', ' . $tgl_indo;
        }
        return $tgl_indo;
    }



    public static function saveMatchToJsonSingular($id)
    {


        $teamId = FootballTeam::where('slug', $id)->first();
        // $match =  FootballMatch::where('slug', $id)->with(['home_team', 'away_team', 'liga'])->first();
        $match =  FootballMatch::where('status', '=', '0')
            ->where('home_team_id', $teamId->id)
            ->orWhere('away_team_id', $teamId->id)

            ->with(['home_team', 'away_team', 'liga'])
            ->first();

        if ($match === null) {
            $match =  FootballMatch::where('home_team_id', $teamId->id)
                ->orWhere('away_team_id', $teamId)
                ->with(['home_team', 'away_team', 'liga'])
                ->first();
        } else {
            $relatedMatch = FootballMatch::where('liga_id', $match->liga_id)
                ->where('id', '!=', $match->id)
                ->orderBy('start_time', 'ASC')->get();
            $match->related = $relatedMatch;
        }


        file_put_contents(public_path() . '/feed/' . $id . '.json', json_encode([
            'success' => true,
            'data' => $match
        ]));
    }
    public  static function saveMatchToJsonAll()
    {
        $data =  FootballMatch::with(['home_team', 'away_team', 'liga'])

            // ->groupBy('date')
            ->orderBy('start_time', 'ASC')
            //   ->where('start_time', '>=', $tanggalHariIni)
            ->where('status', '!=', '1')
            ->get()
            ->groupBy('tanggal_id');


        file_put_contents(public_path() . '/feed/all.json', json_encode([
            'success' => true,
            'data' => $data
        ]));
    }

    public  static function saveLigaToJsonSingular($slug)
    {

        $ligas = FootballLiga::where('slug', $slug)->first();
        if (isset($ligas->id)) {
            $ligas['teams']  = FootballTeam::where('liga_id', 'like', '%' . $ligas->id . '%')->get();
            $ligas['match']  = FootballMatch::where('liga_id', $ligas->id)->with(['home_team', 'away_team', 'liga'])->get()->groupBy('tanggal_id');;


            $data = $ligas;


            file_put_contents(public_path() . '/liga/' . $slug . '.json', json_encode([
                'success' => true,
                'data' => $data
            ]));
        }
    }
    public  static function saveLigaToJsonAll()
    {
        $data = FootballLiga::all();


        file_put_contents(public_path() . '/liga/all.json', json_encode([
            'success' => true,
            'data' => $data
        ]));
    }
}
