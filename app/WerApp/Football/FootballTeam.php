<?php

namespace App\WerApp\Football;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class FootballTeam extends Model
{
    protected $table = 'werapp_football_teams';
    protected $fillable = [
        'id',
        'liga_id',
        'name',
        'logo',
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    protected $casts = [
        'id' => 'string',
        'liga_id' => 'array',
        'liga' => 'array'
    ];
    // protected $appends = [
    //     'liga'
    // ];
    public function liga()
    {
        $ligas = [];
        $array = $this->getAttribute('liga_id');

        if (is_iterable($array) === true) {
            $arx = $array;
        } else {
            $arx = array($array);
        }


        foreach ($arx as $key => $arr) {

            array_push($ligas, FootballLiga::where('id', $arr)->first());
        }
        return $ligas;
        // return 'asdasd';
    }

    // public function getLigaIdAttribute()
    // {
    //     $ligas = [];
    //     return $liga_ids =  $this->getAttributes('liga_id');


    //     foreach ($liga_ids as $ligaId) {
    //         array_push($ligas, FootballLiga::find($ligaId)->first());
    //     }

    //     return $ligas;
    // }

    public function homeMatch()
    {
        return $this->hasOne(FootballMatch::class, 'home_team_id', 'id');
    }
    public function awayMatch()
    {
        return $this->hasOne(FootballMatch::class, 'home_team_id', 'id');
    }

    protected static function booted()
    {
        static::saving(function ($team) {
            $team->slug = Str::slug($team->name);
        });
    }
}
