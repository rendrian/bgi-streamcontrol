const mix = require("laravel-mix");
require("laravel-mix-brotli");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass("resources/sass/app.scss", "public/css")
    .js("resources/js/app.js", "public/js")
    .brotli()

    // .copyDirectory('resources/images', 'public/images')
    // .copyDirectory('resources/json', 'public/json')
    .browserSync({
        proxy: "bgi-streamcontrol.dev",
        port: 3030
    })
    .version()

    .disableNotifications()

    /* Options */
    .options({
        processCssUrls: false,
        watchOptions: {
            ignored: /node_modules/
        }
    });

// mix.autoload({
//     'jquery': ['$', 'window.jQuery', 'jQuery'],
//     'vue': ['Vue', 'window.Vue'],
//     'moment': ['moment', 'window.moment'],
// })
