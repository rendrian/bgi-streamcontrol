<?php

use App\WerApp\Football\FootballLiga;
use Illuminate\Database\Seeder;

use Illuminate\Support\Str;

class LigaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = $this->sampleData();
        foreach ($data as $data) {
            $x = new FootballLiga(['id' => Str::uuid()]);
            $fill = [
                'name' => $data[0],
                'name_alt' => $data[1],
                'logo' => $data[2],
            ];


            $x->fill($fill);
            $x->save();
            print_r($fill['name'] . " added to Ligas \n");
        }
    }

    private function sampleData()
    {
        return array(
            ["Liga Inggris", "English Premier League", "https://1.bp.blogspot.com/-UkqDqPr4Vqs/XtWO0cxDbWI/AAAAAAAAKvY/g27Pb0viDnkNWzPQ0gMtCUHFwjF6H8l-gCLcBGAsYHQ/s1600/EPL.png"],
            ["Liga Italia", "Serie A", "https://1.bp.blogspot.com/-jQC2etR2rZo/XtWPOUaxS3I/AAAAAAAAKx8/KPS_TDGBQjQ2nXuxzNspY6JKVVrOfiTjwCLcBGAsYHQ/s1600/seriea.png"],
            ["Liga Jerman", "Germany Bundesliga", "https://1.bp.blogspot.com/-zn6IzTAIhI0/XtWPaFp2EaI/AAAAAAAAKyg/-pTplRRFxdgTq2YJ4Evy7eazd3ROjCKjQCLcBGAsYHQ/s1600/bundesliga.png"],
            ["Liga Spanyol", "La Liga ", "https://1.bp.blogspot.com/-oUIOqLt3JqI/XtWPoRlmFfI/AAAAAAAAKzk/6hfKCy6eRM4cO4GikcpxTOMwsLw1mZSKgCLcBGAsYHQ/s1600/La%2BLiga.png"],
            ["Liga Prancis", "Ligue 1", "https://1.bp.blogspot.com/-LCQf_zzn02U/XtWP3HhowcI/AAAAAAAAK1A/Q9vAQ4uBia4ug_jbNcB9uvSneRSZqgYbACLcBGAsYHQ/s1600/ligue1.png"],
            ["Liga Champions", "Champions League", "https://1.bp.blogspot.com/-OpzKnU6ue6c/XtWP1yC5EnI/AAAAAAAAK08/sIPeesSrJ1MfreuBUn9iG907Gtdy8UHVQCLcBGAsYHQ/s1600/ucl.png"],
            ["Liga Europa", "Europa League", "https://1.bp.blogspot.com/-fE--8tJ8fXw/XtWP1uLZy-I/AAAAAAAAK04/b6oKOTwXDYQ5e0GEbLadadpfCBKSrb3VgCLcBGAsYHQ/s1600/europa.png"],
            ["Euro 2021", "Euro 2021", "https://1.bp.blogspot.com/-xRhIvjQuBZE/XteC0-Bo4FI/AAAAAAAAK3E/niihhjQvQi4hobwF_wOKtxM9N5bP2M-aQCLcBGAsYHQ/s1600/euro.png"],
        );
    }
}
